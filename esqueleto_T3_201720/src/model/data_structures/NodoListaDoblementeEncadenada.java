package model.data_structures;

public class NodoListaDoblementeEncadenada<T> 
{
	private NodoListaDoblementeEncadenada<T> anterior;
	
	private NodoListaDoblementeEncadenada<T> siguiente;
	
	private T elemento;
	
	public NodoListaDoblementeEncadenada (T pElemento)
	{
		anterior = null;
		siguiente = null;
		elemento = pElemento;
		
	}
	
	public NodoListaDoblementeEncadenada<T> darAnterior()
	{
		return anterior;
	}
	
	public NodoListaDoblementeEncadenada<T> darSiguiente()
	{
		return siguiente;
	}
	
	public void cambiarSiguiente(NodoListaDoblementeEncadenada<T> nSiguiente)
	{
		siguiente = nSiguiente;
	}
	
	public void cambiarAnterior(NodoListaDoblementeEncadenada<T> nAnterior)
	{
		anterior = nAnterior;
	}
	
	public T darElemento()
	{
		return elemento;
	}
	
	

}