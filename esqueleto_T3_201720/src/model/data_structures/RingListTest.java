package model.data_structures;

import java.util.Iterator;

import junit.framework.TestCase;

public class RingListTest extends TestCase {


	RingList<String> lista;

	protected void setUp() throws Exception {
		lista = new RingList<String>();
	}

	public void testIterator()throws Exception {

		lista.add("C");
		lista.add("B");
		lista.add("A");

		Iterator<String> iterador = lista.iterator();

		assertEquals("el elemento no es correto", "A", iterador.next());
		assertEquals("el elemento no es correto", "B", iterador.next());
		assertEquals("hay mas elementos", true, iterador.hasNext());
		assertEquals("el elemento no es correto", "C", iterador.next());
		assertEquals("no hay mas elementos", false, iterador.hasNext());
	}

	public void testGetSize() throws Exception {
		int tamano = lista.getSize();
		assertEquals("el tamaño de la lista no es correcto", 0, tamano);


		lista.add("A");


		tamano = lista.getSize();
		assertEquals("el tamaño de la lista no es correcto", 1, tamano);


		lista.add("B");
		lista.add("C");
		lista.add("D");


		tamano = lista.getSize();
		assertEquals("el tamaño de la lista no es correcto", 4, tamano);

	}

	public void testNext() throws Exception {

		lista.add("A");
		lista.add("B");


		lista.next();
		assertEquals("el elemento no es correcto", "A", lista.getElement());

		lista.next();
		assertEquals("el elemento no es correcto", "B", lista.getElement());

		lista.next();
		assertEquals("el elemento no es correcto", "A", lista.getElement());

		lista.next();
		assertEquals("el elemento no es correcto", "B", lista.getElement());
	}

	public void testPrevious() throws Exception {

		lista.add("A");
		lista.add("B");
		lista.add("C");


		lista.previous();
		assertEquals("el elemento no es correcto", "A", lista.getElement());

		lista.previous();
		assertEquals("el elemento no es correcto", "B", lista.getElement());

		lista.previous();
		assertEquals("el elemento no es correcto", "C", lista.getElement());

		lista.previous();
		assertEquals("el elemento no es correcto", "A", lista.getElement());
	}

	public void testGetElement() throws Exception {

		lista.add("A");


		assertEquals("el elemento no es correcto", "A", lista.getElement());
	}

	public void testExistElement() throws Exception {

		lista.add("A");
		lista.add("B");
		lista.add("C");
		lista.add("D");
		lista.add("E");



		assertEquals("no se encontro el elemento", true, lista.existElement("D"));
		assertEquals("no se encontro el elemento", false, lista.existElement("G"));
	}

	public void testGetElementAtK() throws Exception {

		lista.add("A");
		lista.add("B");
		lista.add("C");
		lista.add("D");
		lista.add("E");



		
		assertEquals("no se encontro el elemento", "C", lista.getElementAtK(2));
		assertEquals("no se encontro el elemento", "E", lista.getElementAtK(4));
		assertEquals("no se encontro el elemento", "D", lista.getElementAtK(8));
			
		

	}

	public void testAdd() throws Exception {

		lista.add("A");



		assertEquals("el elemento no es correcto", "A", lista.getElement());
		int tamano = lista.getSize();
		assertEquals("el tamaño de la lista no es correcto", 1, tamano);

		try {
			lista.add("B");

		} catch (Exception e) {

		}
		assertEquals("el elemento no es correcto", "B", lista.getElement());
		tamano = lista.getSize();
		assertEquals("el tamaño de la lista no es correcto", 2, tamano);
		lista.previous();
		assertEquals("el elemento no es correcto", "A", lista.getElement());



	}

	public void testAddInOrder() throws Exception {

		lista.addInOrder("A");
		lista.addInOrder("B");
		lista.addInOrder("C");
		lista.addInOrder("D");




		assertEquals("el elemento no es correcto", "A", lista.getElement());
		assertEquals("el elemento no es correcto", "A", lista.getElementAtK(0));

		
		assertEquals("el elemento no es correcto", "B", lista.getElementAtK(1));
		assertEquals("el elemento no es correcto", "C", lista.getElementAtK(2));
		assertEquals("el elemento no es correcto", "D", lista.getElementAtK(3));

	
	}

	public void testAddAtEnd() {

		lista.addAtEnd("A");
		lista.addAtEnd("B");
		lista.addAtEnd("C");
		lista.addAtEnd("D");




		assertEquals("el elemento no es correcto", "A", lista.getElement());

		lista.next();
		assertEquals("el elemento no es correcto", "B", lista.getElement());

		lista.next();
		assertEquals("el elemento no es correcto", "C", lista.getElement());

		lista.next();
		assertEquals("el elemento no es correcto", "D", lista.getElement());

		lista.next();
		assertEquals("el elemento no es correcto", "A", lista.getElement());
	}

	public void testAddAtK() throws Exception {

		lista.addAtK("A", 0);
		lista.addAtK("C", 1);
		lista.addAtK("D", 2);
		lista.addAtK("J", 1);

		assertEquals("el elemento no es correcto", "A", lista.getElementAtK(0));
		assertEquals("el elemento no es correcto", "J", lista.getElementAtK(1));
		
		lista.addAtK("V", 6);
		

	}

	public void testDelete() throws Exception {

		lista.add("A");

		assertEquals("no se elimino el elemento", true, lista.delete());
		assertEquals("se encontro el elemento", false, lista.existElement("A"));
		int tamano = lista.getSize();
		assertEquals("el tamaño de la lista no es correcto", 0, tamano);
		
		lista.add("A");
		lista.add("B");
		lista.add("C");
		lista.add("D");
		lista.add("E");

		assertEquals("no se elimino el elemento", true, lista.delete());
		assertEquals("se encontro el elemento", false, lista.existElement("E"));
		tamano = lista.getSize();
		assertEquals("el tamaño de la lista no es correcto", 4, tamano);
		

	}

	public void testDeleteElement() throws Exception {

		lista.add("A");
		assertEquals("no se elimino el elemento", true, lista.deleteElement("A"));
		assertEquals("se encontro el elemento", false, lista.existElement("A"));
		
		lista.add("A");
		lista.add("B");
		lista.add("C");
		lista.add("D");
		lista.add("E");

		assertEquals("no se elimino el elemento", true, lista.deleteElement("C"));
		assertEquals("se encontro el elemento", false, lista.existElement("C"));
		int tamano = lista.getSize();
		assertEquals("el tamaño de la lista no es correcto", 4, tamano);
		assertEquals("el elemento no existe", false, lista.deleteElement("J"));

	}

	public void testDeleteAtK() throws Exception {

		lista.add("A");

		assertEquals("no se elimino el elemento", true, lista.deleteAtK(0));

		int tamano = lista.getSize();
		assertEquals("el tamaño de la lista no es correcto", 0, tamano);


		lista.addInOrder("1");
		lista.addInOrder("2");
		lista.addInOrder("3");
		lista.addInOrder("4");
		lista.addInOrder("5");
		lista.addInOrder("6");
		lista.addInOrder("7");

		assertEquals("no se elimino el elemento", true, lista.deleteAtK(1));
	
			assertEquals("no se elimino el elemento", "3", lista.getElementAtK(1));
		
			
		tamano = lista.getSize();
		assertEquals("no se elimino el elemento", true, lista.deleteAtK(9));
		assertEquals("no se elimino el elemento", "4", lista.getElementAtK(2));

	}

}
