package model.data_structures;

import junit.framework.TestCase;

public class QueueTest extends TestCase {

	Queue<String> cola;
	protected void setUp() throws Exception 
	{
		cola = new Queue<String>();
	}

	public void testEnqueue() {
		cola.enqueue("A");		
		assertEquals("El numero de elementos no es correcto",  1, cola.getSize());
		assertEquals("El elemento no es correcto",  "A", cola.getElement());
		cola.dequeue();
		assertEquals("El numero de elementos no es correcto",  0, cola.getSize());
		assertEquals("El elemento no es correcto", null, cola.getElement());
		assertEquals("El numero de elementos no es correcto", true, cola.isEmpty());
		
		cola.enqueue("A");
		cola.enqueue("B");
		cola.enqueue("C");
		cola.enqueue("D");
		
		assertEquals("El numero de elementos no es correcto",  4, cola.getSize());
		assertEquals("El elemento no es correcto",  "A", cola.dequeue());
		assertEquals("El numero de elementos no es correcto",  3, cola.getSize());
		assertEquals("El elemento no es correcto",  "B", cola.dequeue());
		assertEquals("El numero de elementos no es correcto",  2, cola.getSize());
		
		
		cola.enqueue("Y");
		assertEquals("El numero de elementos no es correcto",  3, cola.getSize());
		assertEquals("El elemento no es correcto",  "C", cola.dequeue());
		assertEquals("El numero de elementos no es correcto",  2, cola.getSize());
		assertEquals("El elemento no es correcto",  "D", cola.dequeue());
		assertEquals("El numero de elementos no es correcto",  1, cola.getSize());
		assertEquals("El elemento no es correcto",  "Y", cola.dequeue());
		assertEquals("El numero de elementos no es correcto",  0, cola.getSize());
		
	}

	public void testDequeue() {
		cola.enqueue("A");
		assertEquals("El elemento no es correcto",  "A", cola.dequeue());
		cola.enqueue("B");
		cola.enqueue("C");
		assertEquals("El elemento no es correcto",  "B", cola.dequeue());
		assertEquals("El elemento no es correcto",  "C", cola.dequeue());
		assertEquals("El elemento no es correcto",  null, cola.dequeue());
		cola.enqueue("D");
		assertEquals("El elemento no es correcto",  "D", cola.dequeue());
		
	}

	public void testIsEmpty() {
		assertEquals("la cola no esta vacia",  true, cola.isEmpty());
		cola.enqueue("A");
		cola.enqueue("B");
		assertEquals("la cola esta vacia",  false, cola.isEmpty());
		cola.dequeue();
		cola.dequeue();
		assertEquals("la cola no esta vacia",  true, cola.isEmpty());
	}

	public void testGetSize() {
		cola.enqueue("A");
		cola.enqueue("B");
		cola.enqueue("C");
		cola.enqueue("D");
		
		assertEquals("El numero de elementos no es correcto",  4, cola.getSize());
		cola.enqueue("o");
		
		assertEquals("El numero de elementos no es correcto",  5, cola.getSize());
		cola.dequeue();
		cola.dequeue();
		cola.dequeue();
		assertEquals("El numero de elementos no es correcto",  2, cola.getSize());
		cola.dequeue();
		cola.dequeue();
		
		
		assertEquals("El numero de elementos no es correcto",  0, cola.getSize());
		assertEquals("El numero de elementos no es correcto",  0, cola.getSize());
		
	}

}
