package model.data_structures;

import junit.framework.TestCase;


public class StackTest extends TestCase 
{

	Stack<String> pila;
	protected void setUp() throws Exception {
		pila = new Stack<String>();
	}

	public void testGetSize() {
		pila.push("A");
		assertEquals("Se agrego un objeto a la pila", 1, pila.getSize());
		pila.pop();
		assertEquals("Se saco un objeto de la pila", 0, pila.getSize());
		pila.push("A");
		pila.push("B");
		pila.push("C");
		assertEquals("Se agrego un objeto a la pila", 3, pila.getSize());
	}

	public void testIsEmpty() {
		
		assertEquals("no hay objetos en la pila", true, pila.isEmpty());
		pila.push("A");
		assertEquals("hay objetos en la pila", false, pila.isEmpty());
		pila.pop();
		assertEquals("no hay objetos en la pila", true, pila.isEmpty());
	}

	public void testPush() {
		pila.push("A");
		assertEquals("Se agrego un objeto a la pila", 1, pila.getSize());
		assertEquals("Se agrego un objeto a la pila", "A", pila.pop());
		pila.push("B");
		pila.push("C");
		assertEquals("Se agrego un objeto a la pila", 2, pila.getSize());
		assertEquals("Se agrego un objeto a la pila", "C", pila.pop());
		pila.push("G");
		pila.push("C");
		pila.push("D");
		pila.push("E");
		assertEquals("Se agrego un objeto a la pila", 5, pila.getSize());
		assertEquals("Se agrego un objeto a la pila", "E", pila.pop());
	}

	public void testPop() {
		
		pila.push("A");
		assertEquals("Se saco un objeto a la pila", "A", pila.pop());
		assertEquals("Se saco un objeto a la pila", 0, pila.getSize());
		
		pila.push("B");
		pila.push("C");
		assertEquals("Se agrego un objeto a la pila", "C", pila.pop());
		assertEquals("Se elimino un objeto a la pila", 1, pila.getSize());
		pila.push("G");
		pila.push("C");
		pila.push("D");
		pila.push("E");
		
		assertEquals("Se agrego un objeto a la pila", "E", pila.pop());
		assertEquals("Se agrego un objeto a la pila", 4, pila.getSize());
	}

}
