package model.logic;

import java.io.*;
import java.util.Iterator;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import model.data_structures.IStack;
import model.data_structures.Queue;
import model.data_structures.RingList;
import model.data_structures.Stack;
import model.vo.StopVO;

import api.ISTSManager;

public class STSManager implements ISTSManager{

	public STSManager()
	{
		cola = new Queue<BusUpdateVO>();
		stops = new RingList<StopVO>();
	}

	private Queue<BusUpdateVO> cola;

	private RingList<StopVO> stops;




	public void readBusUpdate(String rtFile) throws FileNotFoundException
	{
		Queue<BusUpdateVO> updates = new Queue<BusUpdateVO>();
		File[] updateFiles = getChildFiles(rtFile, "json");
		for (int j = 0; j < updateFiles.length; j++) {
			Gson gson = new Gson();
			BufferedReader reader = new BufferedReader(new FileReader(updateFiles[j]));
			gson = new GsonBuilder().create();
			BusUpdateVO[] update = gson.fromJson(reader, BusUpdateVO[].class);
			for (int i = 0; i < update.length; i++)
			{
				updates.enqueue(update[i]);
			}
		}
		cola =  updates;
	}

	private File[] getChildFiles(String directoryPath, String extension) {
		File dirFile = new File(directoryPath);
		return dirFile.listFiles(new FileFilter() {
			@Override
			public boolean accept(File pathname) {
				return pathname.getName().toLowerCase().endsWith("." + extension.toLowerCase())
						|| pathname.isDirectory();
			}
		});
	}

	@Override
	public Stack<StopVO> listStops(Integer tripID) {
		Stack<StopVO> pila = new Stack<StopVO>();

		BusUpdateVO actual = null;
		while (!cola.isEmpty())
		{
			actual = cola.dequeue();
			if(actual.getTripId() == tripID)
			{
				Iterator<StopVO> iterator = stops.iterator();
				while(iterator.hasNext())
				{
					StopVO actualStop = iterator.next();
					Double distance = getDistance(actual.getLatitude(), actual.getLongitude(), actualStop.getStopLat(), actual.getLongitude());

					if(distance <= 70)
					{
						pila.push(actualStop);
					}
				}
			}

		}
		return pila;
	}



	@Override
	public void loadStops(String stopsFile) {

		String cadena;


		FileReader file = null;
		BufferedReader reader = null;
		String datos[] = null;

		try
		{
			file= new FileReader(stopsFile);
			reader = new BufferedReader(file);

			boolean firstHeaderRow = true;

			while((cadena = reader.readLine())!=null)
			{
				if(!firstHeaderRow)
				{
					datos = cadena.split(",");

					int stopId = Integer.parseInt(datos[0].trim());
					Integer stopCode = datos[1].trim().isEmpty() ? null : Integer.parseInt(datos[1].trim());
					String stopName = datos[2].trim();
					String stopDesc = datos[3].trim();
					double stopLat = Double.parseDouble(datos[4].trim());
					double stopLon = Double.parseDouble(datos[5].trim());
					String zoneId = datos[6].trim();
					String stopUrl = datos[7].trim();
					Integer locationType = datos[8].trim().isEmpty() ? null : Integer.parseInt(datos[8].trim());
					String parentStation = "";
					if(datos.length > 9)
					{
						parentStation = datos[9].trim();
					}

					StopVO newStop = new StopVO(stopId, stopCode, stopName, stopDesc, stopLat, stopLon, zoneId, stopUrl, locationType, parentStation);

					stops.addInOrder(newStop);
				}
				else
				{
					firstHeaderRow = false;
				}
			}
		}
		catch (Exception e)
		{
			throw new RuntimeException("No se pudo cargar el archivo. " + e.getMessage(), e);
		}
		finally
		{
			try
			{
				if(file != null)
				{
					file.close();
				}

				if(reader != null)
				{
					reader.close();
				}
			}
			catch(Exception e2)
			{
				throw new RuntimeException("Error al cerrar el archivo. " + e2.getMessage(), e2);
			}
		}

	}

	public double getDistance(double lat1, double lon1, double lat2, double lon2)
	{
		final int R = 6371*1000; // Radious of the earth

		Double latDistance = toRad(lat2-lat1);
		Double lonDistance = toRad(lon2-lon1);
		Double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) +
				Math.cos(toRad(lat1)) * Math.cos(toRad(lat2)) *
						Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
		Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
		Double distance = R * c;

		return distance;


	}

	private Double toRad(Double value)
	{
		int f = 180;
		return value * Math.PI / f;

	}




}
