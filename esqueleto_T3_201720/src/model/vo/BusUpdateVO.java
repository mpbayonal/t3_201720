package model.vo;

import java.sql.Time;

import com.google.gson.annotations.SerializedName;

public class BusUpdateVO 
{
	 // -----------------------------------------------------------------
    // Attributes
    // -----------------------------------------------------------------
	

	@SerializedName("VehicleNo") private String VehicleNo;
	@SerializedName("TripId") private int TripId;
	@SerializedName("RouteNo") private String RouteNo;
	@SerializedName("Direction") private String Direction;
	@SerializedName("Destination") private String Destination;
	@SerializedName("Pattern") private String Pattern;
	@SerializedName("Latitude") private double Latitude;
	@SerializedName("Longitude") private double Longitude;
	@SerializedName("RecordedTime") private String RecordedTime;
	@SerializedName("RouteMap") private UrlRoute RouteMap;
	public BusUpdateVO(String vehicleNo, int tripId, String routeNo, String direction, String destination,
			String pattern, double latitude, double longitude, String recordedTime, UrlRoute routeMap) {
		super();
		VehicleNo = vehicleNo;
		TripId = tripId;
		RouteNo = routeNo;
		Direction = direction;
		Destination = destination;
		Pattern = pattern;
		Latitude = latitude;
		Longitude = longitude;
		RecordedTime = recordedTime;
		RouteMap = routeMap;
	}
	public String getVehicleNo() {
		return VehicleNo;
	}
	public void setVehicleNo(String vehicleNo) {
		VehicleNo = vehicleNo;
	}
	public int getTripId() {
		return TripId;
	}
	public void setTripId(int tripId) {
		TripId = tripId;
	}
	public String getRouteNo() {
		return RouteNo;
	}
	public void setRouteNo(String routeNo) {
		RouteNo = routeNo;
	}
	public String getDirection() {
		return Direction;
	}
	public void setDirection(String direction) {
		Direction = direction;
	}
	public String getDestination() {
		return Destination;
	}
	public void setDestination(String destination) {
		Destination = destination;
	}
	public String getPattern() {
		return Pattern;
	}
	public void setPattern(String pattern) {
		Pattern = pattern;
	}
	public double getLatitude() {
		return Latitude;
	}
	public void setLatitude(double latitude) {
		Latitude = latitude;
	}
	public double getLongitude() {
		return Longitude;
	}
	public void setLongitude(double longitude) {
		Longitude = longitude;
	}
	public String getRecordedTime() {
		return RecordedTime;
	}
	public void setRecordedTime(String recordedTime) {
		RecordedTime = recordedTime;
	}
	public UrlRoute getRouteMap() {
		return RouteMap;
	}
	public void setRouteMap(UrlRoute routeMap) {
		RouteMap = routeMap;
	}
	
	
	
	 // -----------------------------------------------------------------
    // Constructor
    // -----------------------------------------------------------------

	
	
}