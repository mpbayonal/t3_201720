package controller;

import java.io.File;
import java.io.FileNotFoundException;

import model.data_structures.Stack;
import model.exceptions.TripNotFoundException;
import model.logic.STSManager;
import api.ISTSManager;
import model.vo.StopVO;

public class Controller {
	
	/**
	 * Reference to the routes and stops manager
	 */
	private static ISTSManager  manager = new STSManager();

	public static void loadStops() {
		manager.loadStops("data/stops.txt");		
	}

	public static void readBusUpdates() throws FileNotFoundException {
//		File f = new File("data/Buses Service");
//		File[] updateFiles = f.listFiles();
//		for (int i = 0; i < updateFiles.length; i++) {
			manager.readBusUpdate("data/Buses Service");
		//}
	}
	
	public static void listStops(Integer tripId) throws TripNotFoundException, FileNotFoundException
	{
		manager.loadStops("data/stops.txt");
		readBusUpdates();
		Stack<StopVO> pila = (Stack) manager.listStops(tripId);
		while(!pila.isEmpty())
		{
		System.out.println(pila.pop().getStopName());
		}
		
	}
	

}
