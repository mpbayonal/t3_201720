package api;

import java.io.File;
import java.io.FileNotFoundException;

import model.data_structures.IStack;
import model.vo.StopVO;

public interface ISTSManager {

	/**
	 * Reads a file in json format containing bus updates in real time
	 * @param rtFile - The file to read
	 * @throws FileNotFoundException 
	 */
	public void readBusUpdate(String rtFile) throws FileNotFoundException;
	
	public IStack<StopVO> listStops (Integer tripID);

	

	void loadStops(String stopsFile);
}
